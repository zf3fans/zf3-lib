<?php
declare(strict_types=1);

return [
    'console' => [
        'commands' => [
            \Zf3Lib\Lib\Console\DbReloadCommand::class,
        ],
    ],
];
