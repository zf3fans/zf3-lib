<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class ProjectName extends AbstractHelper
{
    public function __invoke(): string
    {
        return defined('PROJECT_NAME') ? PROJECT_NAME : '';
    }
}