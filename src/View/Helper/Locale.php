<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\View\Helper;

use Laminas\I18n\View\Helper\AbstractTranslatorHelper;
use Laminas\ServiceManager\ServiceManager;
use Zf3Lib\Lib\Translator\Translator;

class Locale extends AbstractTranslatorHelper
{
    private string $locale;

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var Translator $translator */
        $translator = $serviceManager->get(Translator::class);
        $this->locale = $translator->getLocale();
    }

    public function __invoke(): string
    {
        return $this->locale;
    }
}