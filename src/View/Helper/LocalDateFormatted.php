<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\View\Helper;

use Laminas\I18n\View\Helper\AbstractTranslatorHelper;
use Zf3Lib\Lib\Helper;

class LocalDateFormatted extends AbstractTranslatorHelper
{
    public function __invoke($datetime = null)
    {
        $translator = $this->getTranslator();
        try {
            $locale = $translator->getLocale();
        } catch (\Exception $e) {
            return '';
        }

        if ($datetime === null) {
            $datetime = new \DateTime();
        } elseif (!($datetime instanceof \DateTime)) {
            try {
                $datetime = new \DateTime($datetime);
            } catch (\Exception $e) {
                return '';
            }
        }

        $dayOfMonth = (int) $datetime->format('d');
        $dayOfWeek  = (int) $datetime->format('w');
        $month      = ((int) $datetime->format('n')) - 1;

        switch ($locale) {
            case LOCALE_EN:
                $ending = Helper\Locale::getOrdinalNumberEnding($dayOfMonth);
                $format = Helper\Locale::MONTHS_SHORT[$locale][$month] .
                    ' ' . $datetime->format('d') . $ending .
                    ' ' . $datetime->format('Y');
                break;
            case LOCALE_RU:
                $format = $datetime->format('d') .
                    ' ' . Helper\Locale::MONTHS_SHORT[$locale][$month] .
                    ' ' . $datetime->format('Y');
                break;
            default:
                return '';
        }
        return Helper\Locale::DAYS_FULL[$locale][$dayOfWeek] . ', ' . $format;
    }
}