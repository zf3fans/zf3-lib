<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\View\Helper;

use Laminas\I18n\View\Helper\AbstractTranslatorHelper;
use Laminas\I18n\Exception;
use Laminas\ServiceManager\ServiceManager;
use Zf3Lib\Lib\Translator\Translator;

class Translate extends AbstractTranslatorHelper
{
    /**
     * @var Translator
     */
    protected $translator;

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var Translator $translator */
        $this->translator = $serviceManager->get(Translator::class);
    }

    public function __invoke(string $message, ?string $textDomain = null, ?string $locale = null) : string
    {
        return $this->translator->translate($message, $textDomain, $locale);
    }
}