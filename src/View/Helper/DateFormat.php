<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class DateFormat extends AbstractHelper
{
    public const DATETIME_FORMAT_NONE   = 0;
    public const DATETIME_FORMAT_SHORT  = 1;
    public const DATETIME_FORMAT_MEDIUM = 2;
    public const DATETIME_FORMAT_LONG   = 3;

    public function __invoke($dateTime,
                             $dateFormat = self::DATETIME_FORMAT_MEDIUM,
                             $timeFormat = self::DATETIME_FORMAT_MEDIUM
    ) {
        try {
            $dt = new \DateTime($dateTime);
        } catch (\Exception $e) {
            return '';
        }
        $format = '';

        switch (LOCALE) {
            case LOCALE_RU:

                switch ($dateFormat) {
                    case self::DATETIME_FORMAT_NONE:
                        $format .= '';
                        break;

                    case self::DATETIME_FORMAT_SHORT:
                    case self::DATETIME_FORMAT_MEDIUM:
                    case self::DATETIME_FORMAT_LONG:
                        $format .= 'Y-d-m';
                        break;
                }

                if ($format && $timeFormat !== self::DATETIME_FORMAT_NONE) {
                    $format .= ' ';
                }

                $format .= match ($timeFormat) {
                    self::DATETIME_FORMAT_NONE => '',
                    self::DATETIME_FORMAT_SHORT, self::DATETIME_FORMAT_MEDIUM => 'H:i',
                    default => 'H:i:s',
                };
                break;

            case LOCALE_EN:
            default:
                switch ($dateFormat) {
                    case self::DATETIME_FORMAT_NONE:
                        $format .= '';
                        break;

                    case self::DATETIME_FORMAT_SHORT:
                        $format .= 'm.d.y';
                        break;

                    case self::DATETIME_FORMAT_MEDIUM:
                    case self::DATETIME_FORMAT_LONG:
                        $format .= 'M d, Y';
                        break;
                }

                if ($format && $timeFormat !== self::DATETIME_FORMAT_NONE) {
                    $format .= ' ';
                }

            $format .= match ($timeFormat) {
                self::DATETIME_FORMAT_NONE => '',
                self::DATETIME_FORMAT_SHORT, self::DATETIME_FORMAT_MEDIUM => 'H:i',
                default => 'H:i:s',
            };
                break;
        }
        return $dt->format($format);
    }
}