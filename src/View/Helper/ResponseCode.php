<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\View\Helper;

use Laminas\I18n\View\Helper\AbstractTranslatorHelper;
use Laminas\ServiceManager\ServiceManager;
use Laminas\Mvc\Application;
use Laminas\Http\PhpEnvironment\Response;

class ResponseCode extends AbstractTranslatorHelper
{
    /**
     * @var ServiceManager
     */
    private $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function __invoke()
    {
        /** @var Application $application */
        $application = $this->serviceManager->get('Application');

        /** @var Response $response */
        $response = $application->getResponse();

        return $response->getStatusCode();
    }
}