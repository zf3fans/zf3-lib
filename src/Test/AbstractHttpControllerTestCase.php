<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Test;

use Laminas\Stdlib\ArrayUtils;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

abstract class AbstractHttpControllerTestCase extends \Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase
{
    public static function setUpBeforeClass(): void
    {
        require_once __DIR__ . '/../../../../public/bootstrap.php';

        parent::setUpBeforeClass();
    }

    protected function setUp(): void
    {
        $configOverrides = [];
        $this->setApplicationConfig(ArrayUtils::merge(
            // Grabbing the full application configuration:
            include __DIR__ . '/../../../../config/app.config.php',
            $configOverrides
        ));
        parent::setUp();
    }

    protected function getServiceManager(): \Laminas\ServiceManager\ServiceManager
    {
        return $this->getApplicationServiceLocator();
    }

    protected function getService(string $serviceName)
    {
        try {
            $service = $this->getServiceManager()->get($serviceName);
        } catch (NotFoundExceptionInterface $e) {
            throw new \RuntimeException("Service {$serviceName} not found: {$e->getMessage()}");
        } catch (ContainerExceptionInterface $e) {
            throw new \RuntimeException("Could not get service {$serviceName} due to exception: {$e->getMessage()}");
        }

        return $service;
    }
}