<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Console;

use Laminas\Db\Adapter\Adapter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zf3Lib\Lib\Db\AbstractDbGateway;
use Zf3Lib\Lib\Db\AdapterManager;

class DbReloadCommand extends AbstractCommand
{
    protected static $defaultName = 'db-reload';
    private const SCHEME_FILENAME = 'scheme.sql';

    private Adapter $adapter;

    public function __construct(
        private readonly AdapterManager $adapterManager,
    )
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $sqlFiles = $this->getSqlFiles();
        try {
            $this->processSqlFiles($sqlFiles);
        } catch (\RuntimeException) {
            // todo: log
            return self::FAILURE;
        }

        return self::SUCCESS;
    }


    // region Gathering Files

    private function getSqlFiles(): array
    {
        $sqlFiles = [];

        $directories = $this->getDirectories();
        foreach ($directories as $directory) {
            $this->addSuitableSqlFilesFromDirectory($directory . '/migration', $sqlFiles);
        }

        return $sqlFiles;
    }

    private function addSuitableSqlFilesFromDirectory(string $directory, array &$dstSqlFiles): void
    {
        if (!file_exists($directory) || !is_dir($directory)) {
            return;
        }

        $this->addSuitableSqlFile($directory . '/' . self::SCHEME_FILENAME, $dstSqlFiles, true);

        $sqlFilesInDirectory = scandir($directory);
        if ($sqlFilesInDirectory === false) {
            return;
        }

        foreach ($sqlFilesInDirectory as $sqlFile) {
            $this->addSuitableSqlFile($directory . '/' . $sqlFile, $dstSqlFiles, false);
        }
    }

    private function addSuitableSqlFile(string $sqlFilePath, array &$dstSqlFiles, bool $acceptDefault): void
    {
        $sqlFilePath = $this->getFileIfSuitable($sqlFilePath, $acceptDefault);
        if ($sqlFilePath === null) {
            return;
        }

        $dstSqlFiles[] = $sqlFilePath;
    }

    private function getFileIfSuitable(string $sqlFilePath, bool $acceptDefault): ?string
    {
        if (!str_ends_with($sqlFilePath, '.sql')) {
            return null;
        }

        if (!$acceptDefault
            && str_ends_with($sqlFilePath, '/' . self::SCHEME_FILENAME)
        ) {
            return null;
        }

        if (!file_exists($sqlFilePath)) {
            return null;
        }

        if (!is_file($sqlFilePath)) {
            return null;
        }

        return $sqlFilePath;
    }

    private function getDirectories(): array
    {
        if (!defined('ROOT_PATH')) {
            trigger_error("ROOT_PATH is not defined");
            return [];
        }

        $basePath = ROOT_PATH;

        $directories = $this->getBundleDirectories($basePath);
        $directories[] = $basePath;

        return $directories;
    }

    private function getBundleDirectories(string $basePath): array
    {
        $vendorDirectory = $basePath . '/vendor/zf3fans';
        $zf3Bundles = scandir($vendorDirectory);
        if ($zf3Bundles === false) {
            return [];
        }

        $directories = [];
        foreach ($zf3Bundles as $zf3Bundle) {
            if (in_array($zf3Bundle, ['.', '..'], true)) {
                continue;
            }

            $zf3BundlePath = $vendorDirectory . '/' . $zf3Bundle;
            if (!is_dir($zf3BundlePath)) {
                continue;
            }

            $directories[] = $zf3BundlePath;
        }

        return $directories;
    }

    // endregion Gathering Files


    // region Process Sql Queries

    private function processSqlFiles(array $sqlFiles): void
    {
        $this->adapter = $this->adapterManager->get(AbstractDbGateway::ADAPTER_NAME_MASTER);

        foreach ($sqlFiles as $sqlFile) {
            $this->processSqlFile($sqlFile);
        }
    }

    private function processSqlFile(string $sqlFile): void
    {
        $content = file_get_contents($sqlFile);
        if ($content === false) {
            return;
        }

        $queries = explode(';', $content);
        foreach ($queries as $query) {

            if (empty($query)) {
                continue;
            }

            try {
                $this->adapter->query($query, Adapter::QUERY_MODE_EXECUTE);
            } catch (\Throwable $e) {
                $ignore = str_contains($e->getMessage(), 'Query was empty');

                if (!$ignore) {
                    throw new \RuntimeException('Could not execute query: ' . $query, 1, $e);
                }
            }
        }
    }

    // endregion Process Sql Queries
}