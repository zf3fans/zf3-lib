<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Console;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;

final class ConsoleApplicationFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): Application
    {
        $app = new Application(
            'Console System',
            '1.0'
        );
        foreach ($this->createCommands($container) as $command) {
            $app->add($command);
        }
        return $app;
    }

    /**
     * @param ContainerInterface $container
     * @return Command[]
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function createCommands(ContainerInterface $container): array
    {
        $commandNames = $this->config($container)['console']['commands'] ?? [];
        return array_map(
            static function (string $commandName) use ($container) {
                return $container->get($commandName);
            },
            $commandNames
        );
    }

    /**
     * @param ContainerInterface $container
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function config(ContainerInterface $container): array
    {
        return $container->get('config');
    }
}
