<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Entity;

use DateTimeImmutable;

interface EntityEditableInterface
{
    public function createdAt(): DateTimeImmutable;
    public function setCreatedAt(DateTimeImmutable $createdAt): static;

    public function updatedAt(): DateTimeImmutable;
    public function setUpdatedAt(DateTimeImmutable $updatedAt): static;
}