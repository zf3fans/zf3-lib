<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Entity;

abstract class AbstractEntity implements EntityInterface
{
    protected int $id;

    public function id(): int
    {
        return $this->id;
    }

    public function isEmpty(): bool
    {
        return $this->id() === 0;
    }

    public function setId(int $id): static
    {
        $this->id = $id;
        return $this;
    }
}