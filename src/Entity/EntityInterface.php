<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Entity;

interface EntityInterface
{
    public function id(): int;
    public function isEmpty(): bool;
    public function setId(int $id): static;
}