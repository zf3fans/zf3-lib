<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Event;

interface EventHandlerInterface
{
    public function handle(EventInterface $event): void;

    public function getEventClass(): string;
}