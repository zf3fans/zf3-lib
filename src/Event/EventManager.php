<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Event;

use Laminas\ServiceManager\ServiceManager;

class EventManager
{
    public function __construct(
        private readonly ServiceManager $sm,
    )
    {
    }

    private function getHandler(string $eventHandlerClass): EventHandlerInterface
    {
        try {
            $handler = $this->sm->get($eventHandlerClass);
        } catch (\Throwable $e) {
            throw new \RuntimeException("Couldn't create event handler by class name {$eventHandlerClass}: {$e->getMessage()}.");
        }

        if (!($handler instanceof EventHandlerInterface)) {
            throw new \RuntimeException("EventHandlerInterface expected, got " . get_debug_type($handler));
        }

        return $handler;
    }

    private static array $handlers = [];

    public function registerHandler(string $eventHandlerClass): void
    {
        $handler = $this->getHandler($eventHandlerClass);
        $eventClass = $handler->getEventClass();

        if (!isset(self::$handlers[$eventClass])) {
            self::$handlers[$eventClass] = [];
        }

        $handlerName = $handler::class;
        if (isset(self::$handlers[$handlerName])) {
            // Если уже есть этот обработчик, ничего не делаем
            return;
        }

        self::$handlers[$eventClass][$handlerName] = $handler;
    }

    public function triggerEvent(EventInterface $event): void
    {
        $eventClass = $event::class;
        $handlers = self::$handlers[$eventClass] ?? [];

        /** @var EventHandlerInterface $handler */
        foreach ($handlers as $handler) {
            $handler->handle($event);
        }
    }
}
