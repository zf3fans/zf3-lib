<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Controller;

use Laminas\ServiceManager\ServiceManager;
use Laminas\Mvc\Controller\AbstractActionController;
use Psr\Container\{
    ContainerExceptionInterface,
    NotFoundExceptionInterface,
};
use Laminas\View\{
    Renderer\PhpRenderer,
    Model\ViewModel,
};
use Laminas\Http\Response as HttpResponse;
use RuntimeException;
use Throwable;

/**
 * @method string translate(string $message, string $textDomain = 'default', ?string $locale = null)
 */
class Controller extends AbstractActionController
{
    protected ServiceManager $serviceManager;
    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    protected function getServiceManager(): ServiceManager
    {
        return $this->serviceManager;
    }

    protected function getService(string $name): mixed
    {
        try {
            $serviceObject = $this->serviceManager->get($name);
        } catch (ContainerExceptionInterface|NotFoundExceptionInterface $e) {
            throw new RuntimeException("Couldn't init service object or get {$name}: {$e->getMessage()}");
        }

        return $serviceObject;
    }

    /**
     * @param int $status
     * @param bool $isBlank
     * @param Throwable|null $exception
     * @return ViewModel
     */
    protected function error(
        int $status,
        bool $isBlank = false,
        ?Throwable $exception = null
    ): ViewModel
    {
        if (PHP_SAPI !== 'cli') {
            $e = $this->getEvent();
            /** @var HttpResponse $response */
            $response = $e->getResponse();
            $response->setStatusCode($status);
        }
        
    
        $this->layout()->setVariables([
            'disable_sidebar' => true
        ]);
    
        $view = new ViewModel();
        
        if ($isBlank) {
            $view->setTemplate('view/blank');
        } else {
            switch ($status) {
                case 403:
                case 404:
                case 500:
                    $view->setTemplate('app/error');
                    if ($exception !== null) {
                        $view->setVariable('exception', $exception);
                    }
                    break;
                default:
                    $view->setTemplate('view/blank');
            }
        }
        return $view;
    }

    /**
     * @return bool
     */
    protected function isAjaxRequest(): bool
    {
        $httpXRequestedWith = $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '';
        return (strtolower($httpXRequestedWith) === 'xmlhttprequest');
    }

    /**
     * @param array $result
     * @return ViewModel
     */
    protected function getJsonView(array $result = []): ViewModel
    {
        $this->layout()->setTemplate('layout/blank');
        return (new ViewModel())
            ->setTemplate('view/json')
            ->setVariable('content', $result);
    }

    /**
     * @param array $variables
     * @return ViewModel
     */
    protected function getView(array $variables = []): ViewModel
    {
        return (new ViewModel())
            ->setVariables($variables);
    }

    protected function getRenderer(): PhpRenderer
    {
        return $this->getService(PhpRenderer::class);
    }
}