<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Controller;

use Zf3Lib\Lib\Entity\EntityInterface;
use Zf3Lib\Lib\Repository;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\Form\FormInterface;
use Zf3Lib\Lib\Repository\EntityAdapterInterface;

trait TEntityCrudController
{
    private function getEntityPrimaryId(): string
    {
        return $this->getEntityName() . '_id';
    }

    private function getEntityName(): string
    {
        return 'entity';
    }

    abstract private function getForm(): FormInterface;

    abstract private function getRepository(): Repository\RepositoryInterface;
    private function getEntityById(int $entityId): EntityInterface
    {
        return $this->getRepository()->getById($entityId);
    }

    abstract private function saveEntity(EntityInterface $entity): int;
    abstract private function getRouteSuccess(): string;

    private function getEntityAdapter(): EntityAdapterInterface
    {
        return $this->getRepository()->hydrator();
    }
    private function entityToArray(EntityInterface $entity): array
    {
        return $this->getEntityAdapter()->toArray($entity);
    }
    private function entityFromArray(array $entityData): EntityInterface
    {
        return $this->getEntityAdapter()->fromArray($entityData);
    }

    // region CRUD

    public function showAction(): ViewModel
    {
        $entityId = (int) $this->params($this->getEntityPrimaryId());
        $entity = $this->getEntityById($entityId);
        if ($entity->id() === 0) {
            return $this->error(Response::STATUS_CODE_404);
        }

        return (new ViewModel())->setVariables([
            $this->getEntityName() => $entity
        ]);
    }

    public function addAction(): ViewModel
    {
        return $this->_editAction(0);
    }

    public function editAction(): ViewModel
    {
        $entityId = (int) $this->params($this->getEntityPrimaryId());
        return $this->_editAction($entityId);
    }

    private function _editAction(int $entityId): ViewModel|Response
    {
        $entity = $this->getEntityById($entityId);
        if ($entityId !== 0 && $entity->id() === 0) {
            return $this->error(Response::STATUS_CODE_404);
        }


        $form = $this->getForm();
        $errors = [];

        if ($this->getRequest()->isPost()) {
            // Уже засабмитили форму
            $post = $this->getRequest()->getPost();
            $form->setData($post);

            if ($form->isValid()) {
                // Ошибок заполнения формы нет, пробуем сохранить
                $formData = $form->getData();
                $entity = $this->entityFromArray($formData);
                try {
                    $entityId = $this->saveEntity($entity);
                    if ($entityId !== 0) {
                        return $this->redirect()->toRoute(
                            $this->getRouteSuccess(),
                            [
                                $this->getEntityPrimaryId() => $entityId
                            ]
                        );
                    } else {
                        $errors[] = "Couldn't to save entity, try again later";
                    }
                } catch (\Throwable $e) {
                    $errors[] = "Couldn't to save entity: {$e->getMessage()}";
                }

            } else {
                // Есть ошибки, нужно что-то исправить
                $errors = $form->getMessages();
            }
        } else {
            // Первый заход на страницу
            $formData = $this->entityToArray($entity);
            $form->setData($formData);
        }

        return (new ViewModel())->setVariables([
            'form'                  => $form,
            'errors'                => $errors,
            $this->getEntityName()  => $entity,
        ]);
    }

    // endregion CRUD
}