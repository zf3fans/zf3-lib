<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Controller\Plugin;

use Zf3Lib\Lib\Translator\Translator;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Laminas\ServiceManager\ServiceManager;

class TranslatePlugin extends AbstractPlugin
{
    private Translator $translator;
    private ServiceManager $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    private function getTranslator(): Translator
    {
        if (!isset($this->translator)) {
            $this->translator = $this->serviceManager->get(Translator::class);
        }
        return $this->translator;
    }

    /**
     * @param string $message
     * @param string $textDomain
     * @param ?string $locale
     * @return string
     */
    public function __invoke(string $message, string $textDomain = 'default', ?string $locale = null): string
    {
        return $this->getTranslator()->translate($message, $textDomain, $locale);
    }
}