<?php
declare(strict_types=1);
namespace Zf3Lib\Lib;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Cache\CacheManager;
use Zf3Lib\Lib\Console\DbReloadCommand;
use Zf3Lib\Lib\Db;
use Zf3Lib\Lib\Translator\{
    Translator,
    TranslatorServiceFactory,
};
use Zf3Lib\Lib\Console\ConsoleApplicationFactory;
use Zf3Lib\Lib\Event\EventManager;
use Zf3Lib\Lib\Helper;
use Laminas\ServiceManager\ServiceManager;
use Laminas\ModuleManager\ModuleManager;
use Symfony\Component\Console\Application as ConsoleApp;

class Module
{
    public function init(ModuleManager $manager): void
    {
        Helper\Locale::init();
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    #[ArrayShape(['factories' => "array", 'invokables' => "string[]"])]
    public function getServiceConfig(): array
    {
        return [
            'factories' => [
                Translator::class => TranslatorServiceFactory::class,
                ConsoleApp::class => ConsoleApplicationFactory::class,

                Db\AdapterManager::class =>
                    fn (ServiceManager $sm) =>
                        new Db\AdapterManager(
                            $sm->get(Db\AbstractDbGateway::ADAPTER_NAME_MASTER),
                        ),
                Cache\CacheManager::class =>
                    fn (ServiceManager $sm) =>
                        new CacheManager(
                            $sm->get('config')['cache'] ?? [],
                        ),
                \Zf3Lib\Lib\Event\EventManager::class =>
                    fn (ServiceManager $sm) =>
                        new \Zf3Lib\Lib\Event\EventManager($sm),

                DbReloadCommand::class =>
                    fn (ServiceManager $sm) =>
                        new DbReloadCommand(
                            $sm->get(Db\AdapterManager::class),
                        ),
            ],
            'invokables' => [
                TranslatorServiceFactory::class => TranslatorServiceFactory::class,
                ConsoleApplicationFactory::class => ConsoleApplicationFactory::class,
            ],
        ];
    }

    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getControllerPluginConfig(): array
    {
        return [
            'factories' => [
                'translate' => fn (ServiceManager $serviceManager) => new \Zf3Lib\Lib\Controller\Plugin\TranslatePlugin($serviceManager),
            ],
        ];
    }

    #[ArrayShape(['invokables' => "string[]", 'factories' => "array"])]
    public function getViewHelperConfig(): array
    {
        return [
            'invokables' => [
                'projectName' => \Zf3Lib\Lib\View\Helper\ProjectName::class,
                'localDateFormatted' => \Zf3Lib\Lib\View\Helper\LocalDateFormatted::class,
            ],
            'factories' => [
                'translate' => fn (ServiceManager $serviceManager) => new \Zf3Lib\Lib\View\Helper\Translate($serviceManager),
                'locale' => fn (ServiceManager $serviceManager) => new \Zf3Lib\Lib\View\Helper\Locale($serviceManager),
                'responseCode' => fn (ServiceManager $serviceManager) => new \Zf3Lib\Lib\View\Helper\ResponseCode($serviceManager),
            ],
        ];
    }
}
