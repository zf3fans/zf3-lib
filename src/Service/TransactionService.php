<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Service;

use Laminas\Db\Adapter\Adapter;
use Zf3Lib\Lib\Db\QueryBuilder;

class TransactionService
{
    protected Adapter           $adapter;
    protected QueryBuilder      $query;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    private function query(): QueryBuilder
    {
        if (!isset($this->query)) {
            $this->query = new QueryBuilder($this->adapter);
        }
        return $this->query;
    }


    private static int $transactionLevel = 0;

    public function transactionBegin(): void
    {
        if (self::$transactionLevel === 0) {
            $this->query()->beginTransaction();
        }
        ++self::$transactionLevel;
    }

    public function transactionCommit(): void
    {
        --self::$transactionLevel;
        if (self::$transactionLevel === 0) {
            $this->query()->commit();
        }
    }

    public function transactionRollback(): void
    {
        --self::$transactionLevel;
        if (self::$transactionLevel === 0) {
            $this->query()->rollback();
        }
    }

    public function onShutdown(): void
    {
        if (self::$transactionLevel !== 0) {
            $this->query()->rollback();
            throw new \RuntimeException("DB transaction was not closed");
        }
        var_dump("111111");die;
    }
}