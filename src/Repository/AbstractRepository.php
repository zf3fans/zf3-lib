<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Repository;

use Laminas\Hydrator\HydratorInterface;
use Zf3Lib\Lib\Db\DbGatewayInterface;
use Zf3Lib\Lib\Db\SearchParams;
use Zf3Lib\Lib\Db\SearchParamsInterface;
use Zf3Lib\Lib\Entity\EntityEditableInterface;
use Zf3Lib\Lib\Entity\EntityInterface;
use Zf3Lib\Lib\Db\AbstractSearchParams;
use Laminas\Db\Sql;
use Zf3Lib\Lib\Helper\Arr;

abstract class AbstractRepository implements RepositoryInterface
{
    protected DbGatewayInterface $dbGateway;
    protected HydratorInterface $hydrator;

    protected function dbGateway(): DbGatewayInterface
    {
        return $this->dbGateway;
    }

    public function primaryKey(): string
    {
        return $this->dbGateway()->primaryKey();
    }

    public function hydrator(): HydratorInterface
    {
        return $this->hydrator;
    }

    protected function getWhere(SearchParamsInterface $searchParams): Sql\Where
    {
        return $this->dbGateway()->getWhereBySearchParams($searchParams);
    }

    protected function getSearchParamsById(int $entityId): SearchParamsInterface
    {
        return (new SearchParams())->setIds([$entityId]);
    }

    public function getEmpty(): EntityInterface
    {
        return $this->hydrator()->hydrate([], new \stdClass());
    }

    public function getById(int $entityId): EntityInterface
    {
        return Arr::first(
            $this->getList(
                $this->getSearchParamsById($entityId)
            )
        ) ?? $this->getEmpty();
    }

    /**
     * @param SearchParamsInterface $searchParams
     * @return array<EntityInterface>
     */
    public function getList(SearchParamsInterface $searchParams): array
    {
        $rawList = $this->dbGateway()->findList(
            $this->getWhere($searchParams),
            $searchParams->orderBy(),
            $searchParams->limit(),
            $searchParams->offset(),
        );

        if (count($rawList) === 0) {
            return [];
        }

        $hydrator = $this->hydrator();
        $list = [];
        foreach ($rawList as $entityData) {
            $entity = $hydrator->hydrate($entityData, new \stdClass());
            $list[$entity->id()] = $entity;
        }

        return $list;
    }

    public function save(EntityInterface &$entity): int
    {
        if ($entity instanceof EntityEditableInterface) {
            $now = new \DateTimeImmutable('now');
            if ($entity->id() === 0) {
                $entity->setCreatedAt($now);
            }
            $entity->setUpdatedAt($now);
        }

        $entityId = $this->dbGateway()->save(
            $this->hydrator()->extract($entity)
        );
        $entity->setId($entityId);

        return $entityId;
    }

    public function delete(EntityInterface $entity): bool
    {
        return $this->dbGateway()->delete(
            //$this->getWhere($this->getSearchParamsById($entity->id()))
            $entity->id()
        ) > 0;
    }

    /**
     * @param array<EntityInterface> $entities
     * @return array
     */
    protected function getIds(array $entities): array
    {
        return array_map(
            static fn (EntityInterface $entity) => $entity->id(),
            $entities
        );
    }
}
