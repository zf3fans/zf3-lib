<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Repository;

use Zf3Lib\Lib\Entity\EntityInterface;

interface EntityAdapterInterface
{
    public function getEmpty(): EntityInterface;
    public function fromArray(array $entityData): EntityInterface;
    public function toArray(EntityInterface $entity): array;
}