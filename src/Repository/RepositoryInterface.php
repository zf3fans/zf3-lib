<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Repository;

use Laminas\Hydrator\HydratorInterface;
use Zf3Lib\Lib\Db\SearchParamsInterface;
use Zf3Lib\Lib\Entity\EntityInterface;

interface RepositoryInterface
{
    public function getEmpty(): EntityInterface;
    public function getById(int $entityId): EntityInterface|null;
    public function getList(SearchParamsInterface $searchParams): array;

    public function delete(EntityInterface $entity): bool;
    public function save(EntityInterface &$entity): int;

    public function hydrator(): HydratorInterface;
    public function primaryKey(): string;
}