<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Repository;

use Zf3Lib\Lib\Entity\EntityInterface;

abstract class AbstractEntityAdapter implements EntityAdapterInterface
{
    abstract protected function _getEmptyEntity(): EntityInterface;
    abstract protected function _getFromArray(array $entityData): EntityInterface;

    public function getEmpty(): EntityInterface
    {
        return $this->_getFromArray([]);
    }

    public function fromArray(array $entityData): EntityInterface
    {
        return $this->_getFromArray($entityData);
    }

    abstract public function toArray(EntityInterface $entity): array;
}