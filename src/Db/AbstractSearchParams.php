<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

use Laminas\Db\Sql;
use InvalidArgumentException;

abstract class AbstractSearchParams implements SearchParamsInterface
{
    protected ?array $ids = null;

    public function ids(): ?array
    {
        return $this->ids;
    }

    public function setIds(?array $ids): static
    {
        if ($ids === null) {
            $this->ids = $ids;
        } else {
            $this->ids = array_map('intval', $ids);
        }
        return $this;
    }

    // region Order & Limit

    public const SORTABLE_FIELD_DEFAULT = '';
    public const SORTABLE_FIELDS = [
    ];
    public const SORT_DIRECTION_DEFAULT = Sql\Select::ORDER_ASCENDING;
    public const SORT_DIRECTIONS = [
        Sql\Select::ORDER_ASCENDING,
        Sql\Select::ORDER_DESCENDING,
    ];

    /**
     * @var array|null
     */
    protected ?array $orderBy = null;

    /**
     * Поля, по которым будет идти сортировка.
     * Представляет собой массив вида ['field' => 'order_direction'],
     * например, ['event_id' => 'ASC', 'dt_publication' => 'DESC']
     * @see Sql\Select::ORDER_ASCENDING
     * @see Sql\Select::ORDER_DESCENDING
     *
     * @return array|null
     */
    public function orderBy(): ?array
    {
        return ($this->orderBy !== null && count($this->orderBy) > 0)
            ? $this->orderBy
            : null;
    }

    /**
     * @param array|null $orderBy
     * @return static
     */
    public function setOrderBy(?array $orderBy): static
    {
        if ($orderBy === null) {
            $this->orderBy = $orderBy;
            return $this;
        }

        foreach ($orderBy as $field => $orderDirection) {
            if (!is_string($field) || !is_string($orderDirection)) {
                throw new InvalidArgumentException("Wrong format of parameter 'orderBy'.");
            }

            if (!in_array($orderDirection, [Sql\Select::ORDER_ASCENDING, Sql\Select::ORDER_DESCENDING], true)) {
                throw new InvalidArgumentException("Unsupported order direction value, {$orderDirection} given.");
            }

            if (!in_array($field, static::SORTABLE_FIELDS, true)) {
                throw new InvalidArgumentException("Field '{$field}' is not sortable.");
            }
        }

        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @var int|null
     */
    protected ?int $limit = null;

    /**
     * @return int|null
     */
    public function limit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     * @return static
     */
    public function setLimit(?int $limit): static
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @var int|null
     */
    protected ?int $offset = null;

    /**
     * @return int|null
     */
    public function offset(): ?int
    {
        return $this->offset;
    }

    /**
     * @param int|null $offset
     * @return static
     */
    public function setOffset(?int $offset): static
    {
        $this->offset = $offset;
        return $this;
    }

    public function pageNumber(): int
    {
        $limit = $this->limit();
        if ($limit === null || $limit <=0) {
            return 1;
        }

        $offset = $this->offset();
        if ($offset === null || $offset < 0) {
            return 1;
        }

        return 1 + (int) floor($offset / $limit);
    }

    public function setLimitOffsetByPage(int $pageNumber, int $limit): static
    {
        if ($limit < 0) {
            $limit = 0;
        }
        $offset = $this->getOffsetByParams($pageNumber, $limit);

        $this->setLimit($limit);
        $this->setOffset($offset);

        return $this;
    }

    public function getOffsetByParams(int $pageNumber, int $limit): int
    {
        if ($pageNumber <= 0) {
            $pageNumber = 1;
        }
        if ($limit < 0) {
            $limit = 0;
        }
        return ($pageNumber - 1) * $limit;
    }

    // endregion Order & Limit
}
