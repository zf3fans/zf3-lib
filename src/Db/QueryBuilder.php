<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

use Closure;
use Laminas\Db\Sql\{Select, TableIdentifier, Where, Sql, Expression};
use Laminas\Db\Adapter\{Adapter, Driver\ConnectionInterface, Driver\ResultInterface};
use Zf3Lib\Lib\Helper;
use RuntimeException;

class QueryBuilder
{
    protected int|null $lastInsertValue = null;

    protected Adapter $adapter;
    protected function getAdapter(): Adapter
    {
        if (!isset($this->adapter)) {
            throw new RuntimeException('DB adapter is not defined yet.');
        }
        return $this->adapter;
    }

    protected function setAdapter(Adapter $adapter): QueryBuilder
    {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     * @return ConnectionInterface
     */
    public function beginTransaction(): ConnectionInterface
    {
        return $this->getAdapter()->getDriver()->getConnection()->beginTransaction();
    }

    /**
     * @return ConnectionInterface
     */
    public function commit(): ConnectionInterface
    {
        return $this->getAdapter()->getDriver()->getConnection()->commit();
    }

    /**
     * @return ConnectionInterface
     */
    public function rollback(): ConnectionInterface
    {
        return $this->getAdapter()->getDriver()->getConnection()->rollback();
    }

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->setAdapter($adapter);
    }

    public function fetchAll($select): ResultInterface
    {
        $sql = new Sql($this->getAdapter());
        return $sql->prepareStatementForSqlObject($select)->execute();
    }

    /**
     * Запрашивает и возвращает одну строку или null если ничего не нашлось
     */
    public function fetchRow(Select $select): ?array
    {
        $results = $this->fetchAll($select->limit(1));
        return $results->current() ?: null;
    }

    /**
     * Запрашивает и возвращает значение одного поля или null если ничего не нашлось
     */
    public function fetchOne(Select $select): string|int|null
    {
        $scalar = null;
        $row = $this->fetchRow($select);
        if ($row) {
            $scalar = reset($row) ?: null;
        }
        return $scalar;
    }

    /**
     * Raw sql string
     */
    public function rawSql(Select $select): string
    {
        return (new Sql($this->getAdapter()))->getSqlStringForSqlObject($select);
    }

    /**
     * Select
     */
    public function select(
        string|array|TableIdentifier $table,
        Where|array|Closure|null     $condition = null
    ): Select
    {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select($table);
        if (is_array($condition) ||
            $condition instanceof Where
        ) {
            $select->where($condition);
        } elseif (is_callable($condition)) {
            $select = $condition($select);
        }
        return $select;
    }

    /**
     * Update
     */
    public function update(
        string|array|TableIdentifier    $table,
        array                           $data,
        Where|Closure|string|array|null $where = null
    ): int
    {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update($table);
        $update->set($data);
        if ($where) {
            $update->where($where);
        }
        $statement = $sql->prepareStatementForSqlObject($update);
        return (int) $statement->execute()->getAffectedRows();
    }

    /**
     * Insert
     */
    public function insert(
        string|array|TableIdentifier $table,
        array                        $data
    ): int
    {
        $sql = new Sql($this->getAdapter());
        $insert = $sql->insert($table)->values($data);
        $sql->prepareStatementForSqlObject($insert)->execute();

        $this->lastInsertValue = $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue()
            // приведение к int'у важно, т.к. getLastGeneratedValue() по факту может вернуть и string
            ? (int) $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue()
            : null;
        return (int) $this->getLastInsertValue();
    }

    public function multiInsert(
        string|array|TableIdentifier $table,
        array                        $data,
        array                        $params = []
    ): bool
    {
        if (count($data) === 0) {
            return false;
        }

        $columns = (array) current($data);
        $columns = array_keys($columns);
        $columnsCount = count($columns);

        $platform = $this->getAdapter()->getPlatform();
        if ($platform === null) {
            throw new RuntimeException("DB platform is not defined.");
        }
        foreach ($columns as & $column) {
            $column = $platform->quoteIdentifier($column);
        }
        unset($column);
        $columns = "(" . implode(',', $columns) . ")";

        $placeholder = array_fill(0, $columnsCount, '?');
        $placeholder = "(" . implode(',', $placeholder) . ")";
        $placeholder = implode(',', array_fill(0, count($data), $placeholder));

        $values = array();
        foreach ($data as $row) {
            foreach ($row as $value) {
                $values[] = $value;
            }
        }

        $table = $platform->quoteIdentifier($table);
        $isIgnore = (bool) Helper\Arr::get($params, 'ignore', false);
        $sqlQuery = $isIgnore ? 'INSERT IGNORE' : 'INSERT';
        $q = "$sqlQuery INTO $table $columns VALUES $placeholder";

        return (bool) $this->getAdapter()->query($q)->execute($values)->getGeneratedValue();
    }

    public function getLastInsertValue(): int|null
    {
        return $this->lastInsertValue;
    }

    /**
     * Delete
     */
    public function delete(
        string|array|TableIdentifier $table,
        array|Where|null             $condition = null
    ): int
    {
        $sql = new Sql($this->getAdapter());
        $delete = $sql->delete($table);
        if (is_array($condition) ||
            $condition instanceof Where
        ) {
            $delete->where($condition);
        }
        $statement = $sql->prepareStatementForSqlObject($delete);
        return $statement->execute()->getAffectedRows();
    }

    /**
     * Count
     */
    public function count(
        string|array|TableIdentifier $table,
        array|Where|null             $condition = null,
    ): Select
    {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select()
            ->columns(['cnt' => new Expression('COUNT(*)')])
            ->from($table);
        if (is_array($condition) || $condition instanceof Where) {
            $select->where($condition);
        }
        return $select;
    }
}