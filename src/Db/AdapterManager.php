<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

use Laminas\Db\Adapter\Adapter;
use RuntimeException;

class AdapterManager
{
    private static array $adapters = [];

    public function __construct(
        Adapter $adapterDbMaster,
    )
    {
        self::$adapters[AbstractDbGateway::ADAPTER_NAME_MASTER] = $adapterDbMaster;
    }

    public function get(string $adapterName): Adapter
    {
        if (!isset(self::$adapters[$adapterName])) {
            throw new RuntimeException("DB adapter {$adapterName} is not defined.");
        }

        return self::$adapters[$adapterName];
    }
}
