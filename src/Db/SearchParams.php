<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

use Laminas\Db\Sql;
use InvalidArgumentException;

class SearchParams extends AbstractSearchParams implements SearchParamsInterface
{
    // region Params

    protected ?array $foreignIds = null;

    public function foreignIds(): ?array
    {
        return $this->foreignIds;
    }

    public function setForeignIds(?array $foreignIds): static
    {
        $this->foreignIds = $foreignIds;
        return $this;
    }


    protected bool $needEnrich = true;

    public function needEnrich(): bool
    {
        return $this->needEnrich;
    }

    public function setNeedEnrich(bool $needEnrich): static
    {
        $this->needEnrich = $needEnrich;
        return $this;
    }

    // endregion Params


    // region Order & Limit

    /**
     * @param array|null $orderBy
     * @return static
     */
    public function setOrderBy(?array $orderBy): static
    {
        if ($orderBy === null) {
            $this->orderBy = $orderBy;
            return $this;
        }

        foreach ($orderBy as $field => $orderDirection) {
            if (!is_string($field) || !is_string($orderDirection)) {
                throw new InvalidArgumentException("Wrong format of parameter 'orderBy'.");
            }

            if (!in_array($orderDirection, static::SORT_DIRECTIONS, true)) {
                if (static::SORT_DIRECTION_DEFAULT === '' || static::SORT_DIRECTION_DEFAULT === null) {
                    throw new InvalidArgumentException("Unsupported order direction value, {$orderDirection} given.");
                }
                $orderDirection = static::SORT_DIRECTION_DEFAULT;
            }

            if (!in_array($field, static::SORTABLE_FIELDS, true)) {
                if (static::SORTABLE_FIELD_DEFAULT === '' || static::SORTABLE_FIELD_DEFAULT === null) {
                    throw new InvalidArgumentException("Field '{$field}' is not sortable.");
                }
                $field = static::SORTABLE_FIELD_DEFAULT;
            }

            $orderBy[$field] = $orderDirection; // на случай если поменяли на дефолтные значения
        }

        $this->orderBy = $orderBy;
        return $this;
    }

    // endregion Order & Limit
}
