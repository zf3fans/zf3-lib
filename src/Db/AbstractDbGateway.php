<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

use Closure;
use Laminas\Db\Sql;
use Laminas\Db\Adapter\{
    Adapter,
    Driver\ConnectionInterface,
    Driver\ResultInterface,
};
use Exception;
use RuntimeException;

abstract class AbstractDbGateway implements DbGatewayInterface
{
    public const TABLE = '';
    public const DB    = 'db';

    public const ALIAS = null;
    public const ADAPTER_NAME_MASTER   = 'App\Db\Master';
    public const DEFAULT_ADAPTER_NAME  = self::ADAPTER_NAME_MASTER;

    protected AdapterManager    $adapterManager;
    protected Adapter           $adapter;
    protected QueryBuilder      $query;
    protected array             $attributes;
    protected string            $scenario;

    public function __construct(AdapterManager $adapterManager)
    {
        $this->adapterManager = $adapterManager;
        $this->setDbAdapter(static::DEFAULT_ADAPTER_NAME);
    }
    
    public function setDbAdapter($adapterName): void
    {
        $this->adapter = $this->adapterManager->get($adapterName);
    }

    public function query(): QueryBuilder
    {
        if (!isset($this->query)) {
            $this->query = new QueryBuilder($this->adapter);
        }
        return $this->query;
    }

    abstract protected function scenarios(): array;

    public function setScenario(string $scenario): static
    {
        $scenarios = $this->scenarios();

        if (!isset($scenarios[$scenario])) {
            throw new RuntimeException("Can't set mandatory scenario {$scenario}");
        }

        $this->scenario = $scenario;
        return $this;
    }

    public function getScenario(): string
    {
        if (!isset($this->scenario)) {
            $this->setScenario('default', true);
        }
        return $this->scenario;
    }

    protected function exchangeArray(array $data): static
    {
        $scenarios = $this->scenarios();
        $this->attributes = array_intersect_key($data, array_flip($scenarios[$this->getScenario()]));
        return $this;
    }

    public function asArray(): array
    {
        if (!isset($this->attributes)) {
            return [];
        }
        return $this->attributes;
    }

    public function primaryKey(): string
    {
        return 'id';
    }

    public function foreignKey(): string
    {
        throw new \RuntimeException("TODO: implement!");
    }

    /**
     * @param Sql\Where|array|int|string $where
     * @return array|null
     */
    public function findOne(
        Sql\Where|array|int|string $where
    ): ?array
    {
        if (!is_array($where) && !($where instanceof Sql\Where)) {
            $where = [
                $this->primaryKey() => $where,
            ];
        }
        $select = $this->query()->select(static::TABLE, $where);
        $row = $this->query()->fetchRow($select);
        return (is_array($row))
            ? $this->prepareFromDb($row)
            : null;
    }

    /**
     * @param Sql\Where|array|null $where
     * @param array|string|Sql\Expression|null $order $order
     * @param int|null $limit
     * @param int|null $offset
     * @return ResultInterface
     */
    public function findAll(
        Sql\Where|array|null $where = null,
        Sql\Expression|array|string|null $order = null,
        ?int $limit = null,
        ?int $offset = null,
    ): ResultInterface
    {
        $select = $this->query()->select(static::TABLE, $where);

        if ($order !== null) {
            $select->order($order);
        }
        if ($limit !== null) {
            $select->limit($limit);
        }
        if ($offset !== null) {
            $select->offset($offset);
        }

        return $this->query()->fetchAll($select);
    }

    public function findList(
        Sql\Where|array|null $where = null,
        Sql\Expression|array|string|null $order = null,
        ?int $limit = null,
        ?int $offset = null,
    ): array
    {
        $result = $this->findAll($where, $order, $limit);

        $list = [];
        foreach ($result as $row) {
            $list[] = $this->prepareFromDb($row);
        }

        return $list;
    }

    protected function prepareFromDb(?array $record): ?array
    {
        return $record;
    }

    public function getWhereBySearchParams(SearchParamsInterface $searchParams): Sql\Where
    {
        $where = new Sql\Where();

        return $where;
    }

    public function select(
        Sql\Where|array|Closure|null $condition = null
    ): Sql\Select
    {
        $table = static::TABLE;
        if (static::ALIAS) {
            $table = array(static::ALIAS => static::TABLE);
        }
        return $this->query()->select($table, $condition);
    }

    public function insert(array $record = []): int
    {
        $record = $this->exchangeArray($record)->asArray();
        if ($this->query()->insert(static::TABLE, $record)){
            return (int) $this->query()->getLastInsertValue();
        }
        return 0;
    }

    /**
     * Пакетная вставка строк
     *
     * Параметр $data представляет собой массив массивов (строк).
     * Возвращает последний вставленный id
     *
     * @param array $data
     * @param array $params
     *
     * @return int
     */
    public function multiInsert(array $data, array $params = []): int
    {
        foreach ($data as & $row) {
            if (!is_array($row)) {
                return 0;
            }
            $row = $this->exchangeArray($row)->asArray();
        }
        unset($row);

        if ($this->query()->multiInsert(static::TABLE, $data, $params)) {
            return (int) $this->query()->getLastInsertValue();
        }

        return 0;
    }

    public function getLastInsertValue(): ?int
    {
        return $this->query()->getLastInsertValue();
    }

    public function update($data, $where = null): int
    {
        if ($where !== null && !is_array($where) && !($where instanceof Sql\Where)) {
            $where = array(
                $this->primaryKey() => $where,
            );
        }
        if (is_array($data)) {
            $data = $this->exchangeArray($data)->asArray();
        }
        return $this->query()->update(static::TABLE, $data, $where);
    }

    /**
     * @param array $record
     * @return int
     */
    public function save(array $record): int
    {
        $recordId = (int) ($record[$this->primaryKey()] ?? 0);
        $where = [$this->primaryKey() => $recordId];

        $this->beginTransaction();
        try {
            $recordStored = $this->findOne($where);
            if ($recordStored === null) {
                $recordId = $this->insert($record);
                if ($recordId === 0) {
                    throw new RuntimeException("Couldn't insert record");
                }
            } else {
                $this->update($record, $where);
            }
            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw new RuntimeException("Couldn't insert record due to exception: {$e->getMessage()}");
        }

        return $recordId;
    }


    public function delete(null|int|string|array|Sql\Where $where = null): int
    {
        if ($where && !is_array($where) && !($where instanceof Sql\Where)) {
            $where = array(
                $this->primaryKey() => $where,
            );
        }
        return $this->query()->delete(static::TABLE, $where);
    }

    public function count(null|array|Sql\Where $where = null): int
    {
        $select = $this->query()->count(static::TABLE, $where);
        return (int) $this->query()->fetchOne($select);
    }

    public function fetchOne(Sql\Select $select): int|string|null
    {
        return $this->query()->fetchOne($select);
    }

    public function fetchRow($select): ?array
    {
        return $this->query()->fetchRow($select);
    }

    public function fetchAll(Sql\Select $select): ResultInterface
    {
        return $this->query()->fetchAll($select);
    }

    public function fetchList(Sql\Select $select): array
    {
        $result = $this->query()->fetchAll($select);
        $list = [];
        foreach ($result as $row) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * @return ConnectionInterface
     */
    protected function beginTransaction(): ConnectionInterface
    {
        return $this->query()->beginTransaction();
    }

    /**
     * @return ConnectionInterface
     */
    protected function commit(): ConnectionInterface
    {
        return $this->query()->commit();
    }

    /**
     * @return ConnectionInterface
     */
    protected function rollback(): ConnectionInterface
    {
        return $this->query()->rollback();
    }
}