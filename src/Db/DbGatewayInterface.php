<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

use Closure;
use Laminas\Db\Adapter\Driver\ConnectionInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\Sql;

interface DbGatewayInterface
{
    public function query(): QueryBuilder;

    public function getScenario(): string;
    public function setScenario(string $scenario): static;

    public function primaryKey(): string;
    public function foreignKey(): string;
    public function findOne(Sql\Where|array|int|string $where): ?array;
    public function findList(
        Sql\Where|array|null $where = null,
        Sql\Expression|array|string|null $order = null,
        ?int $limit = null,
        ?int $offset = null,
    ): array;
    public function getWhereBySearchParams(SearchParamsInterface $searchParams): Sql\Where;

    public function select(Sql\Where|array|Closure|null $condition = null): Sql\Select;
    public function insert(array $record = []): int;
    public function update($data, $where = null): int;
    public function save(array $record): int;
    public function delete(null|int|string|array|Sql\Where $where = null): int;
    public function count(null|array|Sql\Where $where = null): int;

    public function fetchOne(Sql\Select $select): int|string|null;
    public function fetchRow($select): ?array;
    public function fetchAll(Sql\Select $select): ResultInterface;
    public function fetchList(Sql\Select $select): array;
}