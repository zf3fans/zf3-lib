<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Db;

interface SearchParamsInterface
{
    public function ids(): ?array;
    public function setIds(?array $ids): static;

    public function orderBy(): ?array;
    public function setOrderBy(?array $orderBy): static;

    public function limit(): ?int;
    public function setLimit(?int $limit): static;

    public function offset(): ?int;
    public function setOffset(?int $offset): static;

    public function pageNumber(): int;
    public function setLimitOffsetByPage(int $pageNumber, int $limit): static;

    public function getOffsetByParams(int $pageNumber, int $limit): int;
}