<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Base;

use ArrayAccess;
use ArrayIterator;
use Closure;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use Zf3Lib\Lib\Helper\Arr;

abstract class AbstractCollection implements Countable, IteratorAggregate, ArrayAccess
{
    protected string $type;

    protected array $collection = [];

    private function validateType($object): void
    {
        if (!($object instanceof $this->type)) {
            $type = get_debug_type($object);
            throw new InvalidArgumentException("Объект типа " . $type . " не может быть добавлен в коллекцию объектов типа " . $this->type);
        }
    }

    public function add(mixed $object): static
    {
        $this->collection[] = $object;
        return $this;
    }

    public function remove(mixed $object): static
    {
        $offset = array_search($object, $this->collection, true);

        if ($offset !== false) {
            $this->offsetUnset($offset);
        }

        return $this;
    }

    public function isEmpty(): bool
    {
        return empty($this->collection);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->collection);
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->collection[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet(mixed $offset): mixed
    {
        return ($this->offsetExists($offset))
            ? $this->collection[$offset]
            : null;
    }

    /**
     * Возвращает первый элемент коллекции
     */
    public function first(): mixed
    {
        return Arr::first($this->collection);
    }

    /**
     * Возвращает последний элемент коллекции
     */
    public function last(): mixed
    {
        return Arr::last($this->collection);
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        // TODO: решить тестами, а не так
        //$this->validateType($object);

        if ($offset !== null) {
            $this->collection[$offset] = $value;
        } else {
            $this->collection[] = $value;
        }
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->collection[$offset]);
    }

    public function count(): int
    {
        return count($this->collection);
    }

    public function slice(int $offset, ?int $length = null): static
    {
        $this->collection = array_slice($this->collection, $offset, $length);
        return $this;
    }

    public function flush(): static
    {
        $this->collection = [];
        return $this;
    }

    public function map(callable|Closure $callback): array
    {
        return array_map($callback, $this->collection);
    }

    public function toArray(): array
    {
        return $this->collection;
    }
}