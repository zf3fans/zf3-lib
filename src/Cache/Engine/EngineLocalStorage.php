<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache\Engine;

use Zf3Lib\Lib\Cache\KeyParams;

class EngineLocalStorage extends EngineAbstract
{
    public const TYPE = 'local_storage';

    private static array $storage;

    /**
     * Конструктор
     */
    public function __construct(bool $isOn, array $cacheConfig)
    {
        parent::__construct($isOn, $cacheConfig);

        if (!$this->isCacheOn()) {
            return;
        }

        if (!isset(self::$storage)) {
            self::$storage = [];
        }
    }

    public function exist(KeyParams|string $key): bool
    {
        if (self::$storage === null) {
            return false;
        }

        $key = (string) $key;
        return isset(self::$storage[$key]) ?? false;
    }

    /**
     * Возвращает кеш по ключу
     */
    public function get(string|KeyParams $key): mixed
    {
        if (self::$storage === null) {
            return null;
        }

        $key = (string) $key;
        return self::$storage[$key] ?? null;
    }

    /**
     * Записывает новое значение кеша по ключу.
     *
     * В случае успеха возвращает true. Иначе false.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     * @param mixed $data Данные для хранения
     * @param int $ttl Время актуальности кеша в секундах (для данного движка неактуально)
     *
     * @return bool
     */
    public function set(string|KeyParams $key, mixed $data, int $ttl = 0): bool
    {
        // $ttl в local_storage игнорируем
        if (!isset(self::$storage)) {
            return false;
        }

        $key = (string) $key;
        if ($key === '') {
            return false;
        }

        self::$storage[$key] = $data;

        return true;
    }

    /**
     * Удаляет кеш по ключу.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     *
     * @return bool
     */
    public function delete(string|KeyParams $key): bool
    {
        if (self::$storage === null) {
            return false;
        }

        $key = (string) $key;
        if ($key === '') {
            return false;
        }

        unset(self::$storage[$key]);

        return true;
    }

    /**
     * Удаляет весь кеш.
     *
     * @return bool
     */
    public function flush(): bool
    {
        if (self::$storage === null) {
            return false;
        }

        self::$storage = [];
        return true;
    }
}