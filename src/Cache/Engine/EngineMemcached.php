<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache\Engine;
use Exception;
use Zf3Lib\Lib\Cache\KeyParams;
use Memcached;
use RuntimeException;

class EngineMemcached extends EngineAbstract
{
    public const TYPE = 'memcached';

    private static Memcached $memcachedInstance;
    private static EngineLocalStorage $localStorageInstance;

    /**
     * Конструктор
     */
    public function __construct(bool $isOn, array $cacheConfig)
    {
        parent::__construct($isOn, $cacheConfig);

        if (!$this->isCacheOn()) {
            return;
        }

        if (!isset(self::$memcachedInstance))
        {
            $memcachedConfig = $cacheConfig[static::TYPE] ?? [];
            $memcachedServer = (string) ($memcachedConfig['server'] ?? '');
            $memcachedPort   = (int) ($memcachedConfig['port'] ?? 0);
            if ($memcachedServer === '' || $memcachedPort === 0) {
                throw new RuntimeException("Memcached connection parameters are not defined");
            }

            self::$localStorageInstance = new EngineLocalStorage($isOn, $cacheConfig);

            self::$memcachedInstance = new Memcached();
            self::$memcachedInstance->addServer($memcachedServer, $memcachedPort);
        }
    }

    public function exist(KeyParams|string $key): bool
    {
        if (self::$memcachedInstance === null) {
            return false;
        }

        if (self::$localStorageInstance->exist($key)) {
            return true;
        }

        return (bool) self::$memcachedInstance->get((string) $key);
    }

    /**
     * Возвращает кеш по ключу.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     *
     * @return mixed
     */
    public function get(KeyParams|string $key): mixed
    {
        if (self::$memcachedInstance === null) {
            return null;
        }

        if ($data = self::$localStorageInstance->get($key)) {
            return $data;
        }

        try {
            $dataEncoded = self::$memcachedInstance->get((string) $key);
            $data = $this->dataDecode($dataEncoded);
        } catch (Exception) {
            $data = null;
        }

        return $data;
    }

    /**
     * Записывает новое значение кеша по ключу.
     *
     * В случае успеха возвращает true. Иначе false.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     * @param mixed $data Данные для хранения
     * @param int $ttl Время актуальности кеша в секундах
     *
     * @return bool
     */
    public function set(string|KeyParams $key, mixed $data, int $ttl = 0): bool
    {
        if (self::$memcachedInstance === null)
        {
            return false;
        }

        $data_encoded = $this->dataEncode($data);
        if ($data_encoded === null) {
            return false;
        }

        try {
            $result = self::$memcachedInstance->set((string)$key, $data_encoded, $ttl);
            self::$localStorageInstance->set($key, $data, $ttl);
        } catch (Exception) {
            $result = false;
        }

        self::$localStorageInstance->set($key, $data, $ttl);
        return $result;
    }

    /**
     * Удаляет кеш по ключу.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     *
     * @return bool
     */
    public function delete(string|KeyParams $key): bool
    {
        if (self::$memcachedInstance === null) {
            return false;
        }

        try {
            $result = self::$memcachedInstance->delete((string) $key);
        } catch (Exception) {
            $result = false;
        }

        self::$localStorageInstance->delete($key);
        return $result;
    }

    /**
     * Удаляет весь кеш.
     *
     * @return bool
     */
    public function flush(): bool
    {
        if (self::$memcachedInstance === null)
        {
            return false;
        }
        $result = self::$memcachedInstance->flush();
        $result = self::$localStorageInstance->flush() && $result;

        return $result;
    }
}