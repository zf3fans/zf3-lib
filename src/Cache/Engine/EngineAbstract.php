<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache\Engine;

use Exception;
use Zf3Lib\Lib\Cache\KeyParams;

abstract class EngineAbstract implements ICacheEngine
{
    protected bool $isOn;
    final protected function isCacheOn(): bool
    {
        return $this->isOn;
    }

    protected array $cacheConfig;
    final protected function cacheConfig(): array
    {
        return $this->cacheConfig;
    }

    public function __construct(bool $isOn, array $cacheConfig)
    {
        $this->isOn = $isOn;
        $this->cacheConfig = $cacheConfig;
    }

    /**
     * Тип движка. Во всех дочерних классах вы ОБЯЗАНЫ переопределить эту константу
     * @return string
     */
    public const TYPE = '';

    /**
     * Формирует ключ для хранения кеша по параметрам.
     *
     * Если передан один параметр, и это KeyParams, то приводим его к строке (__toString).
     * Если передано несколько параметров стандартных типов, то получаем ключ через статический общедоступный метод.
     *
     * @return string
     */
    public function key(/*$var, ...*/): string
    {
        $args = func_get_args();
        if (count($args) === 1 && $args[0] instanceof KeyParams) {
            $key = (string) $args[0];
        } else {
            $key = KeyParams::toString($args);
        }
        return $key;
    }

    abstract public function exist(string|KeyParams $key): bool;

    /**
     * Возвращает кеш по ключу.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     *
     * @return mixed
     */
    abstract public function get(string|KeyParams $key): mixed;

    /**
     * Записывает новое значение кеша по ключу.
     *
     * В случае успеха возвращает true. Иначе false.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     * @param mixed $data Данные для хранения
     * @param int $ttl Время актуальности кеша в секундах
     *
     * @return bool
     */
    abstract public function set(string|KeyParams $key, mixed $data, int $ttl = 0): bool;

    /**
     * Удаляет кеш по ключу.
     *
     * @param string|KeyParams $key Ключ хранения кеша
     *
     * @return bool
     */
    abstract public function delete(string|KeyParams $key): bool;

    /**
     * Удаляет весь кеш.
     *
     * @return bool
     */
    abstract public function flush(): bool;

    /**
     * Подготавливает данные для хранения в виде кеша
     * @param mixed $dataToCache
     * @return string|null
     */
    protected function dataEncode(mixed $dataToCache): ?string
    {
        $data = null;
        try
        {
            $data = json_encode($dataToCache);
            if ($data === false)
            {
                $data = null;
            }
        }
        catch (Exception) {}

        return $data;
    }

    /**
     * Приводит закешированные данные в исходное состояние
     * @param mixed $dataCached
     * @return mixed|null
     */
    protected function dataDecode(mixed $dataCached): mixed
    {
        if (!is_string($dataCached)) {
            return null;
        }

        $data = null;
        try
        {
            $data = json_decode($dataCached, true);
        }
        catch (Exception) {}

        return $data;
    }
}