<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache\Engine;

use Zf3Lib\Lib\Cache\KeyParams;

interface ICacheEngine
{
    public function key(/*$var, ...*/): string;

    public function exist(string|KeyParams $key): bool;

    public function get(string|KeyParams $key): mixed;

    public function set(string|KeyParams $key, mixed $data, int $ttl = 0): bool;

    public function delete(string|KeyParams $key): bool;

    public function flush(): bool;
}