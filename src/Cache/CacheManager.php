<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache;
use Exception;
use InvalidArgumentException;
use Zf3Lib\Lib\Cache\Engine\EngineLocalStorage;
use Zf3Lib\Lib\Cache\Engine\EngineMemcached;
use RuntimeException;

class CacheManager
{
    public const TTL_1_MINUTE      = 60;
    public const TTL_5_MINUTES     = 300;
    public const TTL_10_MINUTES    = 600;
    public const TTL_15_MINUTES    = 900;
    public const TTL_1_HOUR        = 3600;
    public const TTL_1_DAY         = 86400;
    public const TTL_30_DAYS       = 2592000;

    public function __construct(array $config)
    {
        $this->setIsOn($config['is_on'] ?? false);
        $this->setConfig($config);
        $this->setEngineType($config['type'] ?? '');
    }

    /**
     * Массив поддерживаемых типов движков кеширования
     * @var string[]
     */
    private static array $engineTypes = [
        EngineMemcached::TYPE       => EngineMemcached::class,
        EngineLocalStorage::TYPE    => EngineLocalStorage::class,
    ];

    /**
     * Массив созданных движков кеширования
     * @var array
     */
    private static array $engines = [];

    /**
     * Определяет, есть ли движок кеша указанного типа
     * @param string|null $engineType
     * @return bool
     */
    private function isEngineValid(?string $engineType): bool
    {
        return $engineType !== null && isset(self::$engineTypes[$engineType]);
    }

    private bool $isOn;
    public function isOn(): bool
    {
        return $this->isOn;
    }
    private function setIsOn(bool $isOn): void
    {
        $this->isOn = $isOn;
    }

    private array $config;
    public function config(): array
    {
        return $this->config;
    }
    private function setConfig(array $config): void
    {
        $this->config = $config;
    }

    private string $engineType;
    public function engineType(): string
    {
        return $this->engineType;
    }
    public function setEngineType(string $engineType): static
    {
        if (!$this->isEngineValid($engineType)) {
            throw new InvalidArgumentException("Unsupported engine type given: {$engineType}");
        }
        $this->engineType = $engineType;
        return $this;
    }



    /**
     * Фабрика по производству кешей
     *
     * @return Engine\EngineAbstract|null
     */
    public function getEngine(): ?Engine\EngineAbstract
    {
        if (!$this->isOn()) {
            return null;
        }

        $engineType = $this->engineType();

        $engine = self::$engines[$engineType] ?? null;
        if ($engine === null)
        {
            try {
                $engine = new self::$engineTypes[$engineType]($this->isOn(), $this->config());
                self::$engines[$engineType] = $engine;
            } catch (Exception $e) {
                throw new RuntimeException("Couldn't create cache engine object: {$e->getMessage()}");
            }
        }

        return $engine;
    }

    /**
     * Очищает кеш по ключу
     *
     * Данный метод сделан здесь для простоты использования.
     * @param string|KeyParams $key
     *
     * @return bool
     *@todo Подумать, возможно надо разместить в каком-то другом месте.
     *
     */
    public function clearCache(string|KeyParams $key): bool
    {
        $engine = $this->getEngine();
        if ($engine === null)
        {
            return false;
        }

        return $engine->delete($key);
    }

    /**
     * Сбрасывает весь кеш
     *
     * @return bool
     */
    public function clearCacheAll(): bool
    {
        $engine = $this->getEngine();
        if ($engine === null) {
            return false;
        }

        return $engine->flush();
    }
}