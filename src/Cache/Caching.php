<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache;

use Zf3Lib\Lib\Cache\Engine\EngineAbstract;
use RuntimeException;

trait Caching
{
    private ?string $cacheType = null;

    public function cacheSetType(string $type): void
    {
        $this->cacheType = $type;
    }

    private CacheManager $cacheManager;

    public function cacheManagerSet(CacheManager $cacheManager): void
    {
        $this->cacheManager = $cacheManager;
    }

    /**
     * @return EngineAbstract|null
     */
    public function cache(): ?EngineAbstract
    {
        if (!isset($this->cacheManager)) {
            throw new RuntimeException("Cache manager is not defined yet, use cacheManagerSet method please.");
        }
        return $this->cacheManager->setEngineType($this->cacheType)->getEngine();
    }

    /**
     * @param string|KeyParams $class
     * @param string $method
     * @param array $params
     * @return KeyParams
     */
    public function cacheKey(string|KeyParams $class = '', string $method = '', array $params = []): KeyParams
    {
        if ($class instanceof KeyParams)
        {
            return $class;
        }

        if ($class === '' && $method === '' && empty($params))
        {
            // Немного магии.
            // Типовой случай использования cacheKey() без параметров -
            // например, из метода data-провайдера что-то вроде getListByParams(),
            // когда все 3 аргумента (класс, метод и параметры) очевидны.
            $backtrace = debug_backtrace();
            if (isset($backtrace[1])) {
                // $backtrace[0] <-- текущий метод, т.е. cacheKey()
                // $backtrace[1] <-- как раз то место, откуда вызвали cacheKey() без параметров
                $class  = $backtrace[1]['class'];
                $method = $backtrace[1]['function'];
                $params = $backtrace[1]['args'];
            } else {
                return new KeyParams();
            }
        }

        return new KeyParams($class, $method, $params);
    }

    public function cacheExist(string|KeyParams $key): bool
    {
        $engine = $this->cache();
        if ($engine === null) {
            return false;
        }

        return $engine->exist($key);
    }

    /**
     * @param string|KeyParams $key
     * @return mixed|null
     */
    public function cacheGet(string|KeyParams $key): mixed
    {
        $engine = $this->cache();
        if ($engine === null) {
            return null;
        }

        return $engine->get($key);
    }

    /**
     * @param string|KeyParams $key
     * @param mixed $data
     * @param int $ttl
     * @return bool
     */
    public function cacheSet(string|KeyParams $key, mixed $data, int $ttl = 0): bool
    {
        $engine = $this->cache();
        if ($engine === null) {
            return false;
        }

        return $engine->set($key, $data, $ttl);
    }

    /**
     * @param string|KeyParams $key
     * @return bool
     */
    public function cacheDelete(string|KeyParams $key): bool
    {
        $engine = $this->cache();
        if ($engine === null) {
            return false;
        }

        return $engine->delete($key);
    }
}