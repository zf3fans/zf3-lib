<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Cache;

class KeyParams
{
    private string $keyClass;
    public function keyClass(): string
    {
        return $this->keyClass;
    }

    private string $keyMethod;
    public function keyMethod(): string
    {
        return $this->keyMethod;
    }

    private array $keyParams;
    public function keyParams(): array
    {
        return $this->keyParams;
    }

    public function __construct(string $class = '', string $method = '', array $params = [])
    {
        $this->keyClass  = $class;
        $this->keyMethod = $method;
        $this->keyParams = $params;
    }

    public function toArray(): array
    {
        return [
            $this->keyClass,
            $this->keyMethod,
            $this->keyParams,
        ];
    }

    public function __toString(): string
    {
        if ($this->keyClass === '' && $this->keyMethod === '' && empty($this->params)) {
            return '';
        }
        return self::toString($this->toArray());
    }

    /**
     * @param array $args Плоский массив простых параметров любого размера
     * @return string
     */
    public static function toString(array $args): string
    {
        return md5(serialize($args));
    }
}