<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Http;

use Laminas\Mvc\Application;
use Laminas\Http\PhpEnvironment\Request  as HttpRequest;
use Laminas\Http\PhpEnvironment\Response as HttpResponse;
use Laminas\Http\Header\SetCookie;
use JetBrains\PhpStorm\Pure;

abstract class RequestManagerAbstract
{
    protected Application $application;

    public function __construct(
        Application $application,
    )
    {
        $this->application = $application;
    }


    // region Request

    // region HttpRequest

    protected HttpRequest $httpRequest;

    protected function getHttpRequest(): HttpRequest
    {
        if (!isset($this->httpRequest)) {
            /** @var HttpRequest $request */
            $request = $this->application->getRequest();
            $this->httpRequest = $request;
        }

        return $this->httpRequest;
    }

    protected function getQueryParams(): array
    {
        return $this->getHttpRequest()->getQuery()->toArray();
    }

    protected function getPostParams(): array
    {
        return $this->getHttpRequest()->getPost()->toArray();
    }

    protected function getRequestParams(): array
    {
        $httpRequest = $this->getHttpRequest();
        $requestParams = array_merge(
            $httpRequest->getPost()->toArray() ?? [],
            $httpRequest->getQuery()->toArray() ?? []
        );
        return $requestParams;
    }

    // endregion HttpRequest

    // region Request Params

    #[Pure]
    protected function getRouteParams(): array
    {
        return $this->application->getMvcEvent()->getRouteMatch()?->getParams() ?? [];
    }

    protected function getPostRaw(): ?string
    {
        return file_get_contents('php://input') ?: null;
    }

    // endregion Request Params

    // endregion Request


    // region Response

    #[Pure]
    protected function getHttpResponse(): HttpResponse
    {
        /** @var HttpResponse $httpResponse */
        $httpResponse = $this->application->getResponse();

        return $httpResponse;
    }

    protected function setCookie(string $param, string $value, ?int $ttl = null, ?string $path = null, ?string $domain = null): void
    {
        $cookie = new SetCookie(
            $param,
            $value,
            ($ttl !== null) ? time() + $ttl : null,
            $path ?? '/',
            $domain ?? HTTP_HOST
        );
        $this->getHttpResponse()->getHeaders()?->addHeader($cookie);
    }

    // endregion Response
}