<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Translator;
use Interop\Container\ContainerInterface;
use Zf3Lib\Lib\Translator\Translator as LibTranslator;
use Laminas\I18n\Translator\TranslatorServiceFactory as ZendTranslatorServiceFactory;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Psr\Container\{
    ContainerExceptionInterface,
    NotFoundExceptionInterface,
};

class TranslatorServiceFactory extends ZendTranslatorServiceFactory
{
    /**
     * Create a Translator instance.
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return LibTranslator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): LibTranslator
    {
        // Configure the translator
        $config     = $container->get('config');
        $trConfig   = $config['translator'] ?? [];
        $translator = LibTranslator::factory($trConfig);
        if ($container->has('TranslatorPluginManager')) {
            $translator->setPluginManager($container->get('TranslatorPluginManager'));
        }
        return $translator;
    }

    /**
     * zend-servicemanager v2 factory for creating Translator instance.
     *
     * Proxies to `__invoke()`.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return LibTranslator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function createService(ServiceLocatorInterface $serviceLocator): LibTranslator
    {
        return $this($serviceLocator, LibTranslator::class);
    }
}
