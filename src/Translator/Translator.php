<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Translator;
use Laminas\I18n\Translator\Translator as ZendTranslator;
use Laminas\I18n\Exception;

class Translator extends ZendTranslator
{
    public function getLocale(): string
    {
        if (!defined('LOCALE_SUPPORTED') || !defined('LOCALE_DEFAULT')) {
            return parent::getLocale();
        }

        try {
            $locale = (string) parent::getLocale();
        } catch (Exception\ExtensionNotLoadedException $e) {
            $locale = LOCALE_DEFAULT;
        }

        if (!in_array($locale, LOCALE_SUPPORTED)) {
            $locale = LOCALE_DEFAULT;
        }
        return $locale;
    }
}