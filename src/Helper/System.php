<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

class System
{
    /**
     * @param string $identity
     * @param int    $processLimit
     *
     * @return bool
     */
    public static function isProcessLocked(string $identity, int $processLimit = 1): bool
    {
        $command = "ps aux | grep -v grep | grep -v '>>' | grep 'index.php $identity'";
        if (defined('HTTP_HOST')) {
            $command .= " | grep '" . HTTP_HOST . "'";
        }
        exec($command, $psOutput);
        $psOutput = array_filter($psOutput, static fn ($line) => !str_contains($line, 'sudo -u'));
        return (count($psOutput) > $processLimit);
    }

    public static function removeDir(string $path): void
    {
        if (!is_dir($path)) {
            return;
        }

        $objects = scandir($path);
        foreach ($objects as $object) {
            if (in_array($object, ['.', '..'], true)) {
                continue;
            }

            $subPath = $path . DIRECTORY_SEPARATOR . $object;
            if (is_dir($subPath) && !is_link($subPath)) {
                self::removeDir($subPath);
            } else {
                unlink($subPath);
            }
        }

        rmdir($path);
    }

    public static function mkDir(string $path, int $permissions = 0777, bool $recursive = false): bool
    {
        if (!$recursive) {
            return mkdir($path, $permissions);
        }

        $currentPath = '';
        $pathParts = explode(DIRECTORY_SEPARATOR, $path);
        for ($i = 0, $cnt = count($pathParts); $i < $cnt; $i++) {
            $currentPath .= DIRECTORY_SEPARATOR . $pathParts[$i];
            if (file_exists($currentPath)) {
                if (is_dir($currentPath)) {
                    continue;
                }

                if (is_file($currentPath)) {
                    return false;
                }
            }

            $result = mkdir($currentPath, $permissions);
            if ($result === false) {
                return false;
            }
        }

        return true;
    }
}