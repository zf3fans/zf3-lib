<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

class DateTime
{
    public const DT_EMPTY = '0000-00-00 00:00:00';
    public const DT_ZERO  = '1970-01-01 00:00:00';

    public const DT_FORMAT_YMDHIS       = 'Y-m-d H:i:s';
    public const DT_FORMAT_YMDHI        = 'Y-m-d H:i';
    public const DT_FORMAT_YMD          = 'Y-m-d';
    public const DT_FORMAT_HIDMY        = 'H:i d.m.Y';
    public const DT_FORMAT_FULL         = self::DT_FORMAT_YMDHIS;
    public const DT_FORMAT_FULL_COMPACT = 'YmdHis';
    public const DT_FORMAT_FULL_REVERSE = self::DT_FORMAT_HIDMY;

    public static function getDtOrNull($dtValue, ?\DateTimeZone $timeZone = null): ?\DateTime
    {
        $dt = null;
        try {
            if (!empty($dtValue)) {
                $dt = new \DateTime($dtValue, $timeZone);
            }
        } catch (\Exception $e) {}

        return $dt;
    }

    public static function getDtiOrNull($dtValue, ?\DateTimeZone $timeZone = null): ?\DateTimeImmutable
    {
        $dt = null;
        try {
            if (!empty($dtValue)) {
                $dt = new \DateTimeImmutable($dtValue, $timeZone);
            }
        } catch (\Exception $e) {}

        return $dt;
    }

    public static function between(
        \DateTime|\DateTimeImmutable $dt,
        \DateTime|\DateTimeImmutable $dtFrom,
        \DateTime|\DateTimeImmutable $dtTo
    ): bool
    {
        return ($dt >= $dtFrom && $dt <= $dtTo);
    }
}