<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

use Laminas\Uri\Http as HttpUri;

class Url
{
    public static function getUrl(HttpUri $httpUri, bool $isFull = false): string
    {
        $url = $httpUri->getPath();

        if ($query = $httpUri->getQuery()) {
            $url .= '?' . $query;
        }
        if ($fragment = $httpUri->getFragment()) {
            $url .= '#' . $fragment;
        }

        if ($isFull) {
            $urlBase = '';
            if ($scheme = $httpUri->getScheme()) {
                $urlBase .= $scheme . '://';
            }
            if ($host = $httpUri->getHost()) {
                $urlBase .= $host;
            }
            $url = $urlBase . $url;
        }

        return $url;
    }
}