<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper\Math\Statistics;

use InvalidArgumentException;
use RuntimeException;
use Laminas\Db\Sql\Predicate\Operator;

class Statistics
{
    public function __construct()
    {
        $this->min = +INF;
        $this->max = -INF;
        $this->innerElements = [];
    }

    protected array $innerElements;

    protected function value($element)
    {
        return $element;
    }


    protected int|float $min;

    public function min(): int|float
    {
        if ($this->min === +INF) {
            throw new RuntimeException("Min value is not defined yet.");
        }
        return $this->min;
    }

    protected int|string $keyOfMin;

    public function keyOfMin(): int|string
    {
        if (!isset($this->keyOfMin)) {
            throw new RuntimeException("Key of min value is not defined yet.");
        }
        return $this->keyOfMin;
    }


    protected int|float $max;

    public function max(): int|float
    {
        if ($this->max === -INF) {
            throw new RuntimeException("Max value is not defined yet.");
        }
        return $this->max;
    }

    protected int|string $keyOfMax;

    public function keyOfMax(): int|string
    {
        if (!isset($this->keyOfMax)) {
            throw new RuntimeException("Key of max value is not defined yet.");
        }
        return $this->keyOfMax;
    }


    public function count(): int
    {
        return count($this->innerElements);
    }

    /**
     * Среднее
     */
    public function avg(): float
    {
        return ($this->count() !== 0)
            ? $this->sum() / $this->count()
            : 0.0;
    }

    public function sum(): int|float
    {
        $elementValueFunction = fn ($element) => $this->value($element);
        return array_sum(
            array_map(
                $elementValueFunction,
                $this->innerElements
            ),
        );
    }

    protected function importElement(int|float $element): int|float
    {
        return $element;
    }

    public function account(int|float $element, int|string|null $key = null): void
    {
        $innerElement = $this->importElement($element);
        if ($key !== null) {
            $this->innerElements[$key] = $innerElement;
        } else {
            $this->innerElements[] = $innerElement;
        }

        if (!isset($this->min) || $this->min === +INF || $this->compare($innerElement, $this->min, Operator::OP_LT)) {
            $this->min = $innerElement;
            $this->keyOfMin = $key ?? array_key_last($this->innerElements);
        }
        if (!isset($this->max) || $this->max === -INF || $this->compare($innerElement, $this->max, Operator::OP_GT)) {
            $this->max = $innerElement;
            $this->keyOfMax = $key ?? array_key_last($this->innerElements);
        }
    }

    /**
     * Среднее квадратическое
     * @param float $factor
     * @return float
     */
    public function rootMeanSquare(float $factor = 1.00): float
    {
        $elementSquareFunction = function ($element) {
            return $this->value($element) ** 2;
        };
        return ($this->count() !== 0)
            ? $factor * sqrt(
                array_sum(
                    array_map(
                        $elementSquareFunction,
                        $this->innerElements
                    )
                ) / $this->count()
            )
            : 0.0;
    }

    /**
     * @param int|float $valueCritical
     * @param string $operator
     * @return array
     */
    public function getIndexes(int|float $valueCritical, string $operator = Operator::OP_GTE): array
    {
        return array_keys(
            array_filter(
                $this->innerElements,
                function ($value) use ($valueCritical, $operator) {
                    return $this->compare($value, $valueCritical, $operator);
                }
            )
        );
    }

    /**
     * @param float $diffCritical
     * @return array[]
     */
    public function getIndexGroups(float $diffCritical): array
    {
        $valuePrev = null;
        $indexGroups = [];
        $indexGroupCurrent = [];

        foreach ($this->innerElements as $i => $valueCurr) {

            if ($valuePrev !== null) {
                $diff = abs($valuePrev - $valueCurr);

                // Если различие приемлемое,..
                if ($diff <= $diffCritical) {
                    // ...то добавляем в текущую группу
                    $indexGroupCurrent[] = $i;
                } else {
                    // В противном случае мы должны сформировать новую группу.
                    // При этом если у нас уже была какая-то начатая группа, мы её должны добавить в результирующий массив.
                    if (count($indexGroupCurrent) > 0) {
                        $indexGroups[] = $indexGroupCurrent;
                    }

                    $indexGroupCurrent = [$i];
                }
            }
            $valuePrev = $valueCurr;

        }

        // Если под конец работы у нас осталась начатая группа, мы её тоже должны добавить в результирующий массив.
        if (count($indexGroupCurrent) > 0) {
            $indexGroups[] = $indexGroupCurrent;
        }

        return $indexGroups;
    }

    public function getGroups(float $diffCritical): array
    {
        $indexGroups = $this->getIndexGroups($diffCritical);

        return array_map(
            static fn (array $indexGroup) =>
                array_map(
                    static fn ($index) => $this->innerElements[$index],
                    $indexGroup
                )
            ,
            $indexGroups
        );
    }

    protected function compare(int|float $value1, int|float $value2, string $operator): bool
    {
        return match ($operator) {
            Operator::OP_GTE => ($value1 >=  $value2),
            Operator::OP_GT  => ($value1 >   $value2),
            Operator::OP_EQ  => ($value1 === $value2),
            Operator::OP_NE  => ($value1 !== $value2),
            Operator::OP_LT  => ($value1 <   $value2),
            Operator::OP_LTE => ($value1 <=  $value2),
            default => throw new InvalidArgumentException("Unsupported operator given: {$operator}."),
        };
    }
}