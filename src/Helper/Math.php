<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

use Exception;
use JetBrains\PhpStorm\Pure;

class Math
{
    public const FLOAT_COMPARISON_DEFAULT_VALUE = 0.00001;

    /**
     * Сравнивает два целых числа с указанной точностью.
     * Возвращает bool = равны / не равны
     *
     * @param float|int|null $value1
     * @param float|int|null $value2
     * @param float $precision
     * @return bool
     */
    public static function isEqual(
        float|int|null $value1,
        float|int|null $value2,
        float $precision = self::FLOAT_COMPARISON_DEFAULT_VALUE
    ): bool
    {
        if ($value1 === null && $value2 === null) {
            return true;
        }
        if (($value1 === null && $value2 !== null) || ($value2 === null && $value1 !== null)) {
            return false;
        }

        return (abs($value1 - $value2) < $precision);
    }

    /**
     * Можно ли считать число равным нулю с указанной точностью
     *
     * @param float|int $value
     * @param float $precision
     * @return bool
     */
    public static function isZero(float|int $value, float $precision = self::FLOAT_COMPARISON_DEFAULT_VALUE): bool
    {
        return (abs($value) < $precision);
    }

    /**
     * Находится ли число в указанном диапазоне.
     * Диапазон задаётся двумя числами. Их порядок не важен.
     *
     * @param float|int $value        Проверяемое значение
     * @param float|int $borderValue1 Первая граница диапазона
     * @param float|int $borderValue2 Вторая граница диапазона
     * @param bool $isStrict          Строгое сравнение или включая края
     * @return bool
     */
    public static function isBetween(
        float|int $value,
        float|int $borderValue1,
        float|int $borderValue2,
        bool $isStrict = false,
    ): bool
    {
        $borderValueMin = min($borderValue1, $borderValue2);
        $borderValueMax = max($borderValue1, $borderValue2);

        if ($isStrict) {
            return ($value > $borderValueMin && $value < $borderValueMax);
        }

        return ($value >= $borderValueMin && $value <= $borderValueMax);
    }

    /**
     * Находится ли число в указанном диапазоне.
     * Диапазон задаётся массивом из двух действительных чисел. Их порядок не важен.
     *
     * @param float|int $value
     * @param array $borderValues
     * @param bool $isStrict          Строгое сравнение или включая края
     * @return bool
     */
    #[Pure]
    public static function isBetweenArr(
        float|int $value,
        array $borderValues,
        bool $isStrict = false,
    ): bool
    {
        if (count($borderValues) !== 2) {
            return false;
        }

        [$borderValue1, $borderValue2] = $borderValues;
        return self::isBetween($value, $borderValue1, $borderValue2);
    }

    /**
     * Высчитывает расстояние между двумя точками.
     * Каждая точка задаётся двумя координатами: x и y.
     *
     * @param float|int $x1
     * @param float|int $y1
     * @param float|int $x2
     * @param float|int $y2
     * @return float
     */
    public static function distance(float|int $x1, float|int $y1, float|int $x2, float|int $y2): float
    {
        return sqrt((($x2 - $x1) ** 2) + (($y2 - $y1) ** 2));
    }

    /**
     * Возвращает случайное целое число в указанном диапазоне
     *
     * @param int $min
     * @param int $max
     * @return int
     */
    public static function rand(int $min, int $max): int
    {
        if ($min > $max) {
            [$min, $max] = [$max, $min];
        }

        try {
            return random_int($min, $max);
        } catch (Exception) {
            return $min;
        }
    }
}