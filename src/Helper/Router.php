<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

class Router
{
    public static function getRouteParams(array $appConfig, string $routeName): array
    {
        if ($routeName === '') {
            return [];
        }

        $routeParams = Arr::get($appConfig, 'router.routes', []);
        $routeNameParts = explode('/', $routeName);
        for ($i = 0, $c = count($routeNameParts); $i < $c; $i++) {
            $routeNamePart = $routeNameParts[$i];
            $routeParams = Arr::get($routeParams, $routeNamePart, null);
            if ($routeParams === null) {
                return [];
            }

            if ($i < $c - 1) {
                $routeParams = Arr::get($routeParams ?? [], 'child_routes', []);
            }
        }

        return $routeParams;
    }
}