<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

class Money
{
    public static function printable(int|string $amount): string
    {
        if (is_int($amount)) {
            $amount = (string)$amount;
        }

        $amount = preg_replace('/[^0-9]/', '', $amount);
        if (strlen($amount) <= 4) {
            return $amount;
        }

        $amount = strrev($amount);
        $amount = preg_replace('/([0-9]{3})/', '$1 ', $amount);
        $amount = strrev($amount);

        return $amount;
    }
}