<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

class Locale
{
    public const LOCALE_EN = 'en_EN';
    public const LOCALE_RU = 'ru_RU';
    public const LOCALE_SUPPORTED = [
        self::LOCALE_RU,
        self::LOCALE_EN,
    ];

    public static function init(): void
    {
        if (defined('LOCALE')) {
            return;
        }

        if (!defined('LOCALE_RU')) {
            define('LOCALE_RU', self::LOCALE_RU);
        }
        if (!defined('LOCALE_EN')) {
            define('LOCALE_EN', self::LOCALE_EN);
        }


        if (!defined('LOCALE_SUPPORTED')) {
            define('LOCALE_SUPPORTED', self::LOCALE_SUPPORTED);
        }
        if (!defined('LOCALE_DEFAULT')) {
            define('LOCALE_DEFAULT', self::LOCALE_EN);
        }
        $locale = $_COOKIE['locale'] ?? LOCALE_DEFAULT;
        if (!in_array($locale, LOCALE_SUPPORTED)) {
            $locale = LOCALE_DEFAULT;
        }
        define('LOCALE', $locale);

        switch ($locale) {
            case self::LOCALE_EN:
                setlocale(LC_ALL, 'en_EN', 'en_EN.UTF-8', 'en', 'english');
                break;
            case self::LOCALE_RU:
                setlocale(LC_ALL, 'ru_RU', 'ru_RU.UTF-8', 'ru', 'russian');
                break;
        }
    }

    public const MONTHS_SHORT = [
        self::LOCALE_RU => ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        self::LOCALE_EN => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    ];

    public const MONTHS_FULL = [
        self::LOCALE_RU => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        self::LOCALE_EN => ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    ];

    public const DAYS_SHORT = [
        self::LOCALE_RU => ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        self::LOCALE_EN => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    ];

    public const DAYS_FULL = [
        self::LOCALE_RU => ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        self::LOCALE_EN => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    ];

    public static function decline(int $count, $options = ['день', 'дня', 'дней']): string
    {
        $mod = $count % 10;
        if ($count >= 10 && $count <= 20) {
            $result = $options[2];
        } else if ($mod >= 2 && $mod <= 4) {
            $result = $options[1];
        } else {
            $result = $options[0];
        }
        return $result;
    }

    public static function getOrdinalNumberEnding(int $number): string
    {
        $mod10 = $number % 10;
        $mod100 = $number % 100;
        if ($mod100 >= 11 && $mod100 <= 13) {
            $ending = 'th';
        }
        elseif ($mod10 === 1) {
            $ending = 'st';
        }
        elseif ($mod10 === 2) {
            $ending = 'nd';
        }
        elseif ($mod10 === 3) {
            $ending = 'rd';
        }
        else {
            $ending = 'th';
        }
        return $ending;
    }
}