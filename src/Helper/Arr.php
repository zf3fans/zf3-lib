<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

use ArrayAccess;
use Exception;

class Arr
{
    // region Get One

    public static function get(array|ArrayAccess $array, int|string $field, mixed $default = '')
    {
        if ((is_string($field)) && (($dotPosition = strpos($field, '.')) !== false)) {
            $data = self::get($array, substr($field, 0, $dotPosition));
            $result = self::get($data, substr($field, $dotPosition + 1), $default);
            if ($result !== $default) {
                return $result;
            }
        }
        return $array[$field] ?? $default;
    }

    public static function find(array|ArrayAccess $array, array|callable $conditions, bool $isStrict = false): mixed
    {
        foreach ($array as $row) {
            $isFound = true;

            if (is_array($conditions)) {
                foreach ($conditions as $k => $v) {
                    $isFound = ($isStrict && self::get($row, $k) === $v) || (!$isStrict && self::get($row, $k) == $v);
                    if (!$isFound) {
                        break;
                    }
                }
            } elseif (is_callable($conditions)) {
                $isFound = $conditions($row);
            }
            if ($isFound) {
                return $row;
            }
        }
        return null;
    }

    public static function iget(array|ArrayAccess $array, $field, $default = 0): int
    {
        return (int) self::get($array, $field, $default);
    }

    public static function first(array|ArrayAccess $array, $default = null)
    {
        if (count($array) === 0) {
            return $default;
        }

        return reset($array) ?: $default;
    }

    public static function last(array|ArrayAccess $array, $default = null)
    {
        if (!is_array($array) || count($array) === 0) {
            return $default;
        }

        return end($array) ?: $default;
    }

    public static function offset(array|ArrayAccess $array, int $offset, $default = null)
    {
        if ($offset < 0
            || !is_array($array) || count($array) === 0) {
            return $default;
        }

        $counter = 0;
        foreach ($array as $item) {
            if ($counter++ === $offset) {
                return $item;
            }
        }

        return $default;
    }

    public static function random(array|ArrayAccess $array): mixed
    {
        if (count($array) === 0) {
            return null;
        }

        $index = Math::rand(0, count($array) - 1);
        try {
            return $array[$index] ?? null;
        } catch (Exception) {
            return null;
        }
    }

    // endregion Get One


    // region Output

    public static function output(
        array|ArrayAccess $array,
        string $newLine = '<br />',
        string $resultSymbols = '=>',
        int $tabSymbolCount = 4,
        string $tabSymbol = '&nbsp;',
        int $depth = 0
    ): string
    {
        $output = '';

        if ($depth < 0) {
            $depth = 0;
        }
        $shift = str_repeat($tabSymbol, $tabSymbolCount * $depth);

        foreach ($array as $key => $value) {
            $output .= $shift . $key . ' ' . $resultSymbols . ' ';
            if (is_scalar($value)) {
                $output .= $value . $newLine;
            } else {
                $output .= $newLine;
                $output .= self::output($value, $newLine, $resultSymbols, $tabSymbolCount, $tabSymbol, $depth + 1);
            }
        }

        return $output;
    }

    // endregion Output


    // region Rebuild

    public const MAP_MODE_KEY          = 'key';
    public const MAP_MODE_VALUE        = 'value';
    public const MAP_MODE_KEY_VALUE    = 'key-value';
    public const MAP_MODE_VALUE_VALUE  = 'value-value';

    /**
     * @param array|ArrayAccess $array
     * @param callable          $mapper Функция-обработчик, на вход которой передаётся один или два аргумента
     *                                  в зависимости от параметра $mode: ключ и/или значение элемента массива
     * @param string            $mode   Режим передачи параметров: ключ-значение, только значение, только ключ
     * @return array
     */
    public static function map(array|ArrayAccess $array, callable $mapper, string $mode = self::MAP_MODE_KEY_VALUE): array
    {
        $result = [];

        foreach ($array as $key => $value) {
            $params = [];

            if (in_array($mode, [self::MAP_MODE_KEY, self::MAP_MODE_KEY_VALUE], true)) {
                $params[] = $key;
            } elseif ($mode === self::MAP_MODE_VALUE_VALUE) {
                $params[] = $value;
            }

            if (in_array($mode, [self::MAP_MODE_VALUE, self::MAP_MODE_KEY_VALUE, self::MAP_MODE_VALUE_VALUE], true)) {
                $params[] = $value;
            }

            $result[] = call_user_func_array($mapper, $params);
        }

        return $result;
    }

    public static function pluckKeys(array|ArrayAccess $array, array|ArrayAccess $keys): array
    {
        $result = [];
        foreach($keys as $key) {
            if (isset($array[$key])) {
                $result[$key] = $array[$key];
            }
        }
        return $result;
    }

    public static function flatten(array|ArrayAccess $array): array
    {
        $result = [];
        foreach ($array as $item) {
            if (is_array($item)) {
                foreach ($item as $subItem) {
                    $result[] = $subItem;
                }
            } else {
                $result[] = $item;
            }
        }
        return $result;
    }

    public static function flattenDeep(array|ArrayAccess $array): array
    {
        $result = [];
        foreach ($array as $item) {
            if (is_array($item)) {
                foreach ($item as $subItem) {
                    if (is_array($subItem)) {
                        $result = array_merge($result, self::flattenDeep($subItem));
                    } else {
                        $result[] = $subItem;
                    }
                }
            } else {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param array|ArrayAccess $array Исходный массив
     * @param int|string $key Поле, по которому нужно перестроить массив
     *
     * @return array
     */
    public static function rebuildForKey(array|ArrayAccess $array, int|string $key = 'id'): array
    {
        $result = [];
        if (!is_array($array)) {
            return $result;
        }

        foreach ($array as $row) {
            $field_value = self::get($row, $key);
            if ('' != $field_value) {
                $result[$field_value] = $row;
            }
        }
        return $result;
    }

    public static function groupBy(array|ArrayAccess $array, $groupKey = 'group_id', $itemIdKey = null): array
    {
        $result = [];

        foreach ($array as $row) {
            $groupId = self::get($row, $groupKey);
            if ($itemIdKey) {
                $itemId = self::get($row, $itemIdKey);
                $result[$groupId][$itemId] = $row;
            } else {
                $result[$groupId][] = $row;
            }
        }

        return $result;
    }

    public static function rebuildInTree(
        array|ArrayAccess $array,
        int|string $parentId = 0,
        int $level = 0,
        int|string $key = 'id',
        int|string $keyParent = 'parent_id',
        string $keySubElements = 'sub_elements',
        int $levelLimit = 1000
    ): array
    {
        $tree = [];
        if (!is_array($array) || $level >= $levelLimit) return $tree;

        foreach ($array as $element) {
            if ($element[$keyParent] != $parentId) {
                continue;
            }
            $id = $element[$key];
            $element[$keySubElements] = self::rebuildInTree($array, $id, $level + 1, $key, $keyParent, $keySubElements, $levelLimit);
            $tree[$id] = $element;
        }

        return $tree;
    }

    // endregion Rebuild


    // region Filter

    public const FILTER_FLAG_UNIQUE    = 0b00000001;
    public const FILTER_FLAG_NOT_EMPTY = 0b00000010;
    public const FILTER_FLAGS_DEFAULT  = self::FILTER_FLAG_UNIQUE | self::FILTER_FLAG_NOT_EMPTY;

    public static function filterArray(array $array, $flags = self::FILTER_FLAGS_DEFAULT): array
    {
        if ($flags & self::FILTER_FLAG_NOT_EMPTY) {
            $array = array_filter($array);
        }

        if ($flags & self::FILTER_FLAG_UNIQUE) {
            $array = array_unique($array);
        }

        return $array;
    }

    public static function filterArrayOfString(array $array, $flags = self::FILTER_FLAGS_DEFAULT): array
    {
        $array = self::filterArray($array, $flags);

        foreach ($array as $k => $value) {
            $array[$k] = (string) $value;
        }

        return $array;
    }

    public static function filterArrayOfInt(array $array, $flags = self::FILTER_FLAGS_DEFAULT): array
    {
        $array = self::filterArray($array, $flags);

        foreach ($array as $k => $value) {
            $array[$k] = (int) $value;
        }

        return $array;
    }

    // endregion Filter
}