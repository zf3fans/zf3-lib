<?php
declare(strict_types=1);
namespace Zf3Lib\Lib\Helper;

use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Result
{
    #[ArrayShape(['result' => "bool", 'data' => "array"])]
    public static function result(bool $isSuccess, array $data = []): array
    {
        return [
            'result' => $isSuccess,
            'data'   => $data
        ];
    }

    #[Pure]
    #[ArrayShape(['result' => "bool", 'data' => "array"])]
    public static function success(array $data = []): array
    {
        return self::result(true, $data);
    }

    #[Pure]
    #[ArrayShape(['result' => "bool", 'data' => "array"])]
    public static function fail(array $data = []): array
    {
        return self::result(false, $data);
    }
}
